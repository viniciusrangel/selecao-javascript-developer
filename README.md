![alt text](http://www.descontei.com.br/wp-content/uploads/2015/05/celular-direto.png "Celular Direto")

# Vaga: Javascript Developer

Este é o repositório onde nós vamos conhecer um pouquinho do seu código ;)
Através desse teste vamos analisar o seu estilo de codificação, bibliotecas preferidas, raciocínio lógico, 
entre outros detalhes que poderão fazer a diferença na hora da sua entrevista!

Se possível, insira seu currículo na raiz do projeto também.

---
### O que devo fazer agora? ###

* Faça um fork nesse repositório
* Implemente sua solução para a proposta do teste
* Crie um pull request ao finalizar

---
### Ok, mas... e o teste? ###

Aqui na Celular Direto nós trabalhamos com aplicações completas em AngularJS, que consomem serviços externos e gerenciam as informações 
durante todo o fluxo de navegação do usuário. Por isso, queremos conhecer um pouco da sua experiência com o AngularJS pois aqui você irá trabalhar
90% do seu tempo com ele! ;)

A sua aplicação deverá listar as plataformas da seguinte API: 
```
http://private-59658d-celulardireto2017.apiary-mock.com/plataformas 
```
Com essa listagem, você deverá possibilitar ao usuário navegar pelos respectivos planos vinculados (pelo sku) a cada plataforma. Abaixo segue as URL's da API de planos:

```
http://private-59658d-celulardireto2017.apiary-mock.com/planos/TBT01 (tablet)
http://private-59658d-celulardireto2017.apiary-mock.com/planos/CPT02 (computador)
http://private-59658d-celulardireto2017.apiary-mock.com/planos/WF03 (wi-fi)
```

Costumamos separar essas etapas em rotas diferentes. A última tela deverá ser um formulário simples, e com os seguintes campos: Nome, E-mail, Nascimento, CPF e Telefone.
No botão de submit do formulário, basta printar as informações no console do navegador (inclusive os dados do plano selecionado nas etapas anteriores).

Em resumo, essas são as etapas da aplicação:

* Selecionar plataforma
* Selecionar plano
* Preencher dados pessoais

A parte visual fica ao seu critério, faça como achar melhor. Não esqueça de criar um readme na raiz do projeto nos orientando sobre como rodar sua aplicação, ok? Lembrando que quanto menos comandos precisarmos para isso, melhor! ;)

---
### O que iremos analisar ###

* Boa organização de código
* Automatização de tarefas (Grunt, Gulp, Webpack)
* Reutilização e Componentização
* Organização dos estilos (OOCSS, BEM, SMACSS)
* UI/UX

---

## Boa sorte! ##